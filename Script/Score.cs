﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private float score = 0.0f;
    private int difficulty = 1;
    private int max = 10;
    private int next = 10;

    public Text t;

    // Update is called once per frame
    void Update()
    {
        if (score => next)
            levelUp();
        score += Time.deltaTime * difficulty;
        t.text = ((int)score).ToString();
    }

    void levelUp()
    {
        if (difficulty == max)
            return;
        next *= 2;
        difficulty++;
        GetComponent<PlayerMotor>().setSpeed(difficulty);
        Debug.Log(difficulty);
    }
}
