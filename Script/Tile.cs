﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public GameObject[] prefab;

    private Transform playertrans;
    private float spawnZ = -10.1f;
    private float length = 10.1f;
    private float safe = 13.1f;
    private int tileonscreen = 6;
    private int lastPrefab = 0;

    private List<GameObject> active;

    // Start is called before the first frame update
    private void Start()
    {
        active = new List<GameObject>();
        playertrans = GameObject.FindGameObjectWithTag("Player").transform;
        for (int i = 0; i < tileonscreen; i++)
        {
            if (i < 4)
                spawnTile(0);
            else
                spawnTile();
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (playertrans.position.z - safe > (spawnZ - tileonscreen * length))
        {
            spawnTile();
            deleteTile();
        }
    }

    private void spawnTile(int prefabindex = -1)
    {
        GameObject go;
        if (prefabindex == -1)
            go = Instantiate(prefab[randomPrefab()]) as GameObject;
        else
            go = Instantiate(prefab[prefabindex]) as GameObject;
        go.transform.SetParent(transform);
        go.transform.position = Vector3.forward * spawnZ;
        spawnZ += length;
        active.Add(go);
    }

    private void deleteTile()
    {
        Destroy(active[0]);
        active.RemoveAt(0);
    }

    private int randomPrefab()
    {
        if (prefab.Length <= 2)
            return 0;
        int randomIndex = lastPrefab;
        while(randomIndex == lastPrefab)
        {
            randomIndex = Random.Range(0, prefab.Length);
        }
        lastPrefab = randomIndex;
        return randomIndex;
    }
}
