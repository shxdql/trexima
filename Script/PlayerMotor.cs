﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour
{
    private CharacterController control;
    private Vector3 move;
    private float verticalVelocity = 0.0f;
    private float gravity = 15.0f;
    private float speed = 5.0f;
    private float animDuration = 3.0f;
    // Start is called before the first frame update
    void Start()
    {
        control = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        move = Vector3.zero;

        if(Time.time < animDuration)
        {
            control.Move(Vector3.forward * speed * Time.deltaTime);
            return;
        }

        if (control.isGrounded)
        {
            verticalVelocity = -0.5f;
        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }

        // X - Kiri Kanan
        move.x = Input.GetAxisRaw("Horizontal") * speed;
        // Y - Loncat
        move.y = verticalVelocity;
        // Z - Maju
        move.z = speed;

        control.Move(move * Time.deltaTime);
    }

    public void setSpeed(float mod)
    {
        speed = 5.0f + mod;
    }
}
