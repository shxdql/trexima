﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam : MonoBehaviour
{
    private Transform look;
    private Vector3 offset;
    private Vector3 move;

    private float transition = 0.0f;
    private float animDuration = 3.0f;
    private Vector3 animOffSet = new Vector3(0, 5, 5);
    // Start is called before the first frame update
    void Start()
    {
        look = GameObject.FindGameObjectWithTag("Player").transform;
        offset = transform.position - look.position;
    }

    // Update is called once per frame
    void Update()
    {
        move = look.position + offset;
        // X
        move.x = -1;
        // Y
        move.y = Mathf.Clamp(move.y, 1, 5);

        if(transition > 1.0f)
        {
            transform.position = move;
        }
        else
        {
            // Animation at the start of the game
            transform.position = Vector3.Lerp(move + animOffSet, move, transition);
            transition += Time.deltaTime * 1 / animDuration;
        }
    }
}
